# frozen_string_literal: true

class Weather
  attr_reader :logger
  attr_accessor :client

  def initialize(client, logger: Logger.new(STDOUT))
    @logger = logger
    @client = client
  end

  # @return [String]
  def get_weather
    @weather = JSON.parse(Net::HTTP.get(client))
    description + ' ' + temp + ' Kelvin'
  end

  # @return [String]
  def description
    @weather['weather'][0]['description']
  end

  # @return [String]
  def temp
    @weather['main']['temp'].to_s
  end
end
